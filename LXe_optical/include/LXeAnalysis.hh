#include "globals.hh"
#include "g4root.hh"
#include <vector>
#include <string>
#include <unordered_map>
#include <typeinfo>

// #include "TFile.h"
// #include "TH1F.h"
// #include "TTree.h"

#include "G4Track.hh"
#include "G4ThreeVector.hh"
#include "G4LorentzVector.hh"

typedef std::unordered_map<std::string, int> reverse_list;

class G4Run;
class G4Event;
class G4DynamicParticle;
class LXeDetectorConstruction;

class LXeAnalysis
{

public:
  static LXeAnalysis *getInstance();

private:
  LXeAnalysis();

  //TFile *fRootFile;
  //TTree *data;

public:
  ~LXeAnalysis();

  void InitRun(G4String fFileName);
  //void BeginOfRun(G4String RootFileName);
  void EndOfRun();

  void BeginOfEvent(const G4Event *event);
  void EndOfEvent(const G4Event *event);

  void SetVerbose(G4int val) { fVerbose = val; };
  G4int GetVerbose() const { return fVerbose; };

  //void SetOutFileCut(G4String cut) { fFileName = cut; };
//   G4String GetOutFileCut() const { return fOutFileCut; };

//   void SetHitsInfo(G4int hitsOn) { fHitsInfo = hitsOn; };
//   G4int GetHitsInfo() const { return fHitsInfo; };

  G4String GetFileName() { return fFileName; }


 private:
//   LXeEventMessenger* fEventMessenger;
  const LXeDetectorConstruction* fDetector;
  static LXeAnalysis* fManager;

  G4int fScintCollID;
  G4int fPMTCollID;

  G4int fVerbose;

  G4int fPMTThreshold;

  G4bool fForcedrawphotons;
  G4bool fForcenophotons;

  G4int fHitCount;
  G4double globalTime; 
  std::vector<double> v_pmt_t;
  std::vector<double> v_pmt_id;
  std::vector<double> v_Crystal_EDep;
  std::vector<int> CrystalPMT_NumActive;
  G4int fPhotonCount_Scint;
  G4int fPhotonCount_Ceren;
  G4int fAbsorptionCount;
  G4int fBoundaryAbsorptionCount;

  G4double fTotE;

  // These only have meaning if totE > 0
  // If totE = 0 then these won't be set by EndOfEventAction
  G4ThreeVector fEWeightPos;
  G4ThreeVector fReconPos;  // Also relies on hitCount>0
  G4ThreeVector fConvPos;   // true (initial) converstion position
  G4bool fConvPosSet;
  G4ThreeVector fPosMax;
  G4double fEdepMax;

  G4int fPMTsAboveThreshold;
  G4String fFileName;
  };

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
