#include "LXeAnalysis.hh"
#include "LXePMTHit.hh"
#include "LXeScintHit.hh"
#include "LXeDetectorConstruction.hh"
#include "LXeRun.hh"
#include "globals.hh"
#include "G4UnitsTable.hh"
#include "G4ProcessManager.hh"
#include "G4ProcessVector.hh"
#include "G4Run.hh"
#include "G4RunManager.hh"
#include "G4Event.hh"
#include "G4DynamicParticle.hh"
#include "G4PrimaryVertex.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4Track.hh"
#include "G4TrackStatus.hh"
#include "G4VProcess.hh"
#include "G4Ions.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleWithCuts.hh"
#include "G4ParticleTypes.hh"
#include "G4ParticleTable.hh"
#include "G4ios.hh"
#include "G4SDManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4VHitsCollection.hh"
#include "G4AnalysisMessenger.hh" 
#include "G4UnitsTable.hh"
#include "G4EventManager.hh"
#include "G4SystemOfUnits.hh"
#include "G4Trajectory.hh"
#include "G4TrajectoryContainer.hh"
#include "G4UImanager.hh"
#include "G4VVisManager.hh"
// #include <TFile.h>
// #include <TH1D.h>
// #include <TTree.h>
#include "G4SystemOfUnits.hh"

LXeAnalysis *LXeAnalysis::fManager = 0;

LXeAnalysis *LXeAnalysis::getInstance()
{
  if (!fManager)
  {
    fManager = new LXeAnalysis();
  }
  return fManager;
}

LXeAnalysis::LXeAnalysis()
{
  //fMessenger = new SABREAnalysisMessenger(this);
}

void LXeAnalysis::InitRun(G4String fFileName = "out")
{
    G4AnalysisManager *analysisManager = G4AnalysisManager::Instance();
    G4cout << "Using " << analysisManager->GetType() << G4endl;

    // Default settings
    analysisManager->SetVerboseLevel(1);
    // set root file name
   // G4String RootFileName = fFileName + ".root";
    analysisManager->SetFileName(fFileName);
    G4int nbins   = 1000;
    G4double vmin = 0.;
    G4double vmax = 1.;
    
    // 0
    analysisManager->CreateH1("0", "dummy", nbins, vmin, vmax);
    // 1
    analysisManager->CreateH1("hits_per_event", "hits per event", nbins, vmin,
                                vmax);
    // 2
    analysisManager->CreateH1("hits above threshold",
                                "hits per event above threshold", nbins, vmin,
                                vmax);
    // 3
    analysisManager->CreateH1("scintillation", "scintillation photons per event",
                                nbins, vmin, vmax);
    // 4
    analysisManager->CreateH1("Cerenkov", "Cerenkov photons per event", nbins,
                                vmin, vmax);
    // 5
    analysisManager->CreateH1("absorbed", "absorbed photons per event", nbins,
                                vmin, vmax);
    // 6
    analysisManager->CreateH1("boundary absorbed",
                                "photons absorbed at boundary per event", nbins,
                                vmin, vmax);
    // 7=
    analysisManager->CreateH1(
        "E_dep", "energy deposition in scintillator per event", nbins, vmin, vmax);

    analysisManager->CreateNtuple("data", "tree");
    analysisManager->CreateNtupleIColumn("CrystalPMT_NumActive", CrystalPMT_NumActive);
    analysisManager->CreateNtupleDColumn("CrystalPMTHit_T",v_pmt_t);
    analysisManager->CreateNtupleDColumn("CrystalPMTHit_PMTID",v_pmt_id);
    analysisManager->CreateNtupleDColumn("Crystal_EDep", v_Crystal_EDep); 
    analysisManager->FinishNtuple();

}

void LXeAnalysis::BeginOfEvent(const G4Event *event)
{
    G4AnalysisManager *analysisManager = G4AnalysisManager::Instance();
    fHitCount                = 0;
    fPhotonCount_Scint       = 0;
    fPhotonCount_Ceren       = 0;
    fAbsorptionCount         = 0;
    fBoundaryAbsorptionCount = 0;
    fTotE                    = 0.0;
    v_pmt_t.clear();
    v_pmt_id.clear();
    v_Crystal_EDep.clear();

    fConvPosSet = false;
    fEdepMax    = 0.0;

    fPMTsAboveThreshold = 0;
}

void LXeAnalysis::EndOfEvent(const G4Event* anEvent)
{
  G4SDManager* SDman = G4SDManager::GetSDMpointer();


  if(fScintCollID <=0)
      fScintCollID = SDman->GetCollectionID("scintCollection");
  if(fPMTCollID <=0)
      fPMTCollID = SDman->GetCollectionID("pmtHitCollection");

  // G4cout<<fScintCollID<<" scint count"<<G4endl;
  // G4cout<<fPMTCollID<<" pmt count"<<G4endl;
  G4TrajectoryContainer* trajectoryContainer =
  anEvent->GetTrajectoryContainer();

  G4int n_trajectories = 0;
  if(trajectoryContainer)
    n_trajectories = trajectoryContainer->entries();


  LXeScintHitsCollection* scintHC = 0;
  LXePMTHitsCollection* pmtHC     = 0;
  G4HCofThisEvent* hitsCE         = anEvent->GetHCofThisEvent();

  // Get the hit collections

  if(hitsCE)
  {
    if(fScintCollID >= 0)
    {
      scintHC = (LXeScintHitsCollection*) (hitsCE->GetHC(fScintCollID));
    }
    if(fPMTCollID >= 0)
    {
      pmtHC = (LXePMTHitsCollection*) (hitsCE->GetHC(fPMTCollID));
    }
  }
  // G4cout<<hitsCE<<" hit count"<<G4endl;
  // G4cout<<fScintCollID<<" scint count"<<G4endl;
  // G4cout<<fPMTCollID<<" pmt count"<<G4endl;

  // Hits in scintillator
  if(scintHC)
  {
    size_t n_hit = scintHC->entries();
    //G4cout<<" "<<n_hit<<" deposits in crystal"<<G4endl;
    G4ThreeVector eWeightPos(0.);
    G4double edep;
    G4double edepMax = 0;
    // G4cout<<n_hit<<G4endl;
    for(size_t i = 0; i < n_hit; ++i)
    {  // gather info on hits in scintillator
      // G4cout<<((*scintHC)[i])->GetPos()<<G4endl;
      edep = (*scintHC)[i]->GetEdep();
      v_Crystal_EDep.push_back(edep);
      fTotE += edep;
      eWeightPos +=
        (*scintHC)[i]->GetPos() * edep;  // calculate energy weighted pos
      if(edep > edepMax)
      {
        edepMax = edep;  // store max energy deposit
        G4ThreeVector posMax = (*scintHC)[i]->GetPos();
        fPosMax              = posMax;
        fEdepMax             = edep;
      }
    }

   G4AnalysisManager::Instance()->FillH1(7, fTotE);

    if(fTotE == 0.)
    {
      if(fVerbose > 0)
        G4cout << "No hits in the scintillator this event." << G4endl;
    }
    else
    {
      // Finish calculation of energy weighted position
      eWeightPos /= fTotE;
      fEWeightPos = eWeightPos;
      if(fVerbose > 0)
      {
        G4cout << "\tEnergy weighted position of hits in LXe : "
               << eWeightPos / mm << G4endl;
      }
    }
    if(fVerbose > 0)
    {
      G4cout << "\tTotal energy deposition in scintillator : " << fTotE / keV
             << " (keV)" << G4endl;
    }
  }

  CrystalPMT_NumActive.clear();
  if(pmtHC)
  {
    G4ThreeVector reconPos(0., 0., 0.);
    size_t pmts = pmtHC->entries();
    //G4cout<<" "<<pmts<<" deposits in pmt"<<G4endl;
    // Gather info from all PMTs
    for(size_t i = 0; i < pmts; ++i)
    {
      fHitCount += (*pmtHC)[i]->GetPhotonCount();
      reconPos += (*pmtHC)[i]->GetPMTPos() * (*pmtHC)[i]->GetPhotonCount();
      globalTime = (*pmtHC)[i]->GetGlobalTime();//(*pmtHC)[i]->GetLocalTime();//
      v_pmt_t.push_back(globalTime);
      v_pmt_id.push_back((*pmtHC)[i]->GetDetN());
      if((*pmtHC)[i]->GetPhotonCount() >= fPMTThreshold)
      {
        ++fPMTsAboveThreshold;
      }
      else
      {  // wasn't above the threshold, turn it back off
        (*pmtHC)[i]->SetDrawit(false);
      }
    }

    // G4AnalysisManager::Instance()->FillH1(1, fHitCount);
    // G4AnalysisManager::Instance()->FillH1(2, fPMTsAboveThreshold);    

    if(fHitCount > 0)
    {  // don't bother unless there were hits
      CrystalPMT_NumActive.push_back(1);
      reconPos /= fHitCount;
      if(fVerbose > 0)
      {
        G4cout << "\tReconstructed position of hits in LXe : " << reconPos / mm
               << G4endl;
      }
      fReconPos = reconPos;
    }
    else
    {
      CrystalPMT_NumActive.push_back(0);
    }
    pmtHC->DrawAllHits();
  }

  // G4AnalysisManager::Instance()->FillH1(3, fPhotonCount_Scint);
  // G4AnalysisManager::Instance()->FillH1(4, fPhotonCount_Ceren);
  // G4AnalysisManager::Instance()->FillH1(5, fAbsorptionCount);
  // G4AnalysisManager::Instance()->FillH1(6, fBoundaryAbsorptionCount);
  G4AnalysisManager::Instance()->AddNtupleRow();
  

  if(fVerbose > 0)
  {
    // End of event output. later to be controlled by a verbose level
    G4cout << "\tNumber of photons that hit PMTs in this event : " << fHitCount
           << G4endl;
    G4cout << "\tNumber of PMTs above threshold(" << fPMTThreshold
           << ") : " << fPMTsAboveThreshold << G4endl;
    G4cout << "\tNumber of photons produced by scintillation in this event : "
           << fPhotonCount_Scint << G4endl;
    G4cout << "\tNumber of photons produced by cerenkov in this event : "
           << fPhotonCount_Ceren << G4endl;
    G4cout << "\tNumber of photons absorbed (OpAbsorption) in this event : "
           << fAbsorptionCount << G4endl;
    G4cout << "\tNumber of photons absorbed at boundaries (OpBoundary) in "
           << "this event : " << fBoundaryAbsorptionCount << G4endl;
    G4cout << "Unaccounted for photons in this event : "
           << (fPhotonCount_Scint + fPhotonCount_Ceren - fAbsorptionCount -
               fHitCount - fBoundaryAbsorptionCount)
           << G4endl;
  }

  // update the run statistics
  LXeRun* run = static_cast<LXeRun*>(
    G4RunManager::GetRunManager()->GetNonConstCurrentRun());

  run->IncHitCount(fHitCount);
  run->IncPhotonCount_Scint(fPhotonCount_Scint);
  run->IncPhotonCount_Ceren(fPhotonCount_Ceren);
  run->IncEDep(fTotE);
  run->IncAbsorption(fAbsorptionCount);
  run->IncBoundaryAbsorption(fBoundaryAbsorptionCount);
  run->IncHitsAboveThreshold(fPMTsAboveThreshold);

  // If we have set the flag to save 'special' events, save here
  // if(fPhotonCount_Scint + fPhotonCount_Ceren < fDetector->GetSaveThreshold())
  // {
  //   G4RunManager::GetRunManager()->rndmSaveThisEvent();
  // }

}