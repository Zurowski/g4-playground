//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file optical/LXe/src/NaIQFSetup.cc
/// \brief Implementation of the NaIQFSetup class
//
//
#include "globals.hh"

#include "NaIQFSetup.hh"

#include "G4LogicalSkinSurface.hh"
#include "G4LogicalBorderSurface.hh"

#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

NaIQFSetup::NaIQFSetup(G4RotationMatrix *pRot,
                             const G4ThreeVector &tlate,
                             G4LogicalVolume *pMotherLogical,
                             G4bool pMany,
                             G4int pCopyNo,
                             G4Material *pScint,
                             LXeDetectorConstruction* c
                             )
  //Pass info to the G4PVPlacement constructor
  :G4PVPlacement(pRot,tlate,
                 //Temp logical volume must be created here
                 new G4LogicalVolume(new G4Box("temp",1,1,1),
                                     G4Material::GetMaterial("Vacuum"),
                                     "temp",0,0,0),
                 "housing",pMotherLogical,pMany,pCopyNo,pScint),fConstructor(c)
{
  CopyValues();

  G4double nai_x=2.54 *cm;
  G4double nai_y=2.54 *cm;
  G4double nai_z=2.54 *cm;

  G4double teflon_thick = 0.75 *mm;

  G4double wrap_x=nai_x-0.01*teflon_thick;
  G4double wrap_y=nai_y+2.*teflon_thick;
  G4double wrap_z=nai_z+2.*teflon_thick;

  G4double innerRadius_house = 0.*cm;
  G4double outerRadius_house = 0.8*nai_x;
  G4double height_house = nai_x+3.*teflon_thick;
  G4double startAngle_house = 0.*deg;
  G4double spanningAngle_house = 360.*deg;

  G4double innerRadius_shell = 0.*cm;
  G4double outerRadius_shell = outerRadius_house + 0.5 *cm;

  //****************************************************************
    // need to define the crystal wrapping surface
  G4String crystalSurfname("CrystalSurf");
  G4OpticalSurface *CrystalSurf = new G4OpticalSurface(crystalSurfname);
  G4MaterialPropertiesTable *MPT_SurfNaI = new G4MaterialPropertiesTable();
  CrystalSurf->SetType(dielectric_metal);
  CrystalSurf->SetFinish(ground);
  CrystalSurf->SetModel(unified);
  CrystalSurf->SetSigmaAlpha(0); //No microfacets
  G4double phEn[2] = {1.0 * eV, 7.0 * eV};
  G4double refl[2] = {0.96, 0.96};
  //I'll model PTFE as an ideal Lambertian reflector.
  G4double twozeros[2] = {0., 0.};
  MPT_SurfNaI->AddProperty("REFLECTIVITY", phEn, refl, 2);
  MPT_SurfNaI->AddProperty("SPECULARLOBECONSTANT", phEn, twozeros, 2);
  MPT_SurfNaI->AddProperty("SPECULARSPIKECONSTANT", phEn, twozeros, 2);
  MPT_SurfNaI->AddProperty("BACKSCATTERCONSTANT", phEn, twozeros, 2);
  CrystalSurf->SetMaterialPropertiesTable(MPT_SurfNaI);
 
  //*************************** housing and scintillator
  // glass housing
  fHouse = new G4Tubs("house_tube",innerRadius_house,outerRadius_house,
                    height_house/2,startAngle_house,spanningAngle_house);
  fHouse_log = new G4LogicalVolume(fHouse,
                                     G4Material::GetMaterial("Glass"),
                                     "house_log",0,0,0);
  G4RotationMatrix* rm_house = new G4RotationMatrix();
  // rm_house->rotateY(-90*deg);
  // new G4PVPlacement(rm_house,G4ThreeVector(),fHouse_log,"housing",
  //                                fWrap_log,false,0);
 
  
  fScint_box = new G4Box("scint_box",nai_x/2.,nai_y/2.,nai_z/2.);
  // fWrap_box = new G4Box("wrap_box",wrap_x/2.,wrap_y/2.,
  //                          wrap_z/2.);
  G4Box* outer_box = new G4Box("outer_box",wrap_x/2.,wrap_y/2.,wrap_z/2.);
  fWrap_box =  new G4SubtractionSolid("wrap_box",outer_box,fScint_box);
  //fWrap_box = = new G4Box("wrap_box",wrap_x/2.,wrap_y/2.,wrap_z/2.);
 
  fScint_log = new G4LogicalVolume(fScint_box,pScint,
                                   "scint_log",0,0,0);
  fWrap_log = new G4LogicalVolume(fWrap_box,
                                     G4Material::GetMaterial("Teflon"),
                                     "wrap_log",0,0,0);

  G4PVPlacement *crys_phys = new G4PVPlacement(0,G4ThreeVector(),fScint_log,"scintillator",
                                 fHouse_log,false,0);
  rm_house->rotateY(-90*deg);
  G4PVPlacement *wrap_phys = new G4PVPlacement(rm_house,G4ThreeVector(),fWrap_log,"wrap",
                                 fHouse_log,false,0);
  new G4LogicalBorderSurface("surf_Crystal_Wrap", crys_phys, wrap_phys,CrystalSurf);
  // try switching mother volumes


  // Al shell
  fShell = new G4Tubs("shell_tube",outerRadius_house,outerRadius_shell,
                    height_house/2,startAngle_house,spanningAngle_house);
  fShell_log = new G4LogicalVolume(fShell,
                                     G4Material::GetMaterial("Al"),
                                     "shell_log",0,0,0);
  new G4PVPlacement(0,G4ThreeVector(),fShell_log,"shell",
                                 fHouse_log,false,0);
  // need to add al reflectivity info
 
  //****************** Build PMTs
  G4double innerRadius_pmt = 23. *mm;
  G4double height_pmt = 0.6*mm;
  G4double startAngle_pmt = 0.*deg;
  G4double spanningAngle_pmt = 360.*deg;
 
  fPmt = new G4Box("pmt_tube",innerRadius_pmt/2,innerRadius_pmt/2,
                    height_pmt);
 
  //the "photocathode" is a metal slab at the back of the glass that
  //is only a very rough approximation of the real thing since it only
  //absorbs or detects the photons based on the efficiency set below
  fPhotocath = new G4Box("pmt_tube",innerRadius_pmt/2,innerRadius_pmt/2,
                    height_pmt/2);
 
  fPmt_log = new G4LogicalVolume(fPmt,G4Material::GetMaterial("Glass"),
                                 "pmt_log");
  fPhotocath_log = new G4LogicalVolume(fPhotocath,
                                       G4Material::GetMaterial("Al"),
                                       "photocath_log");
 
  new G4PVPlacement(0,G4ThreeVector(0,0,-height_pmt/2),
                                    fPhotocath_log,"photocath",
                                    fPmt_log,false,0);
 
  //***********Arrange pmts around the outside of housing**********

  G4double dx = height_house/fNx;
  G4double dy = height_house/fNy;
  G4double dz = height_house/fNz;
 
  G4double x,y,z;
  G4double xmin = -height_house/2 - dx/2.;
  G4double ymin = -height_house/2 - dy/2.;
  G4double zmin = -height_house/2 - dz/2.;
  G4int k=0;
 
  // z = -fScint_z/2. - height_pmt;      //front
  // PlacePMTs(fPmt_log,0,x,y,dx,dy,xmin,ymin,fNx,fNy,x,y,z,k);

  // G4RotationMatrix* rm_z = new G4RotationMatrix();
  // rm_z->rotateY(180*deg);
  // z = fScint_z/2. + height_pmt;       //back
  // PlacePMTs(fPmt_log,rm_z,x,y,dx,dy,xmin,ymin,fNx,fNy,x,y,z,k);
 
  G4RotationMatrix* rm_y2 = new G4RotationMatrix();
  rm_y2->rotateY(180*deg);
  z = height_house/2+ height_pmt;      //right
  PlacePMTs(fPmt_log,rm_y2,x,y,dx,dy,xmin,ymin,fNx,fNy,x,y,z,k);

  G4RotationMatrix* rm_y1 = new G4RotationMatrix();
  z = -height_house/2 - height_pmt;      //left
  PlacePMTs(fPmt_log,0,x,y,dx,dy,xmin,ymin,fNx,fNy,x,y,z,k);


 
  // G4RotationMatrix* rm_x1 = new G4RotationMatrix();
  // rm_x1->rotateX(90*deg);
  // y = -fScint_y/2. - height_pmt;     //bottom
  // PlacePMTs(fPmt_log,rm_x1,x,z,dx,dz,xmin,zmin,fNx,fNz,x,y,z,k);

  // G4RotationMatrix* rm_x2 = new G4RotationMatrix();
  // rm_x2->rotateX(-90*deg);
  // y = fScint_y/2. + height_pmt;      //top
  // PlacePMTs(fPmt_log,rm_x2,x,z,dx,dz,xmin,zmin,fNx,fNz,x,y,z,k);
 
  VisAttributes();
  SurfaceProperties();

  SetLogicalVolume(fHouse_log);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void NaIQFSetup::CopyValues(){
  fScint_x=fConstructor->GetScintX();
  fScint_y=fConstructor->GetScintY();
  fScint_z=fConstructor->GetScintZ();
  fD_mtl=fConstructor->GetHousingThickness();
  fNx=fConstructor->GetNX();
  fNy=fConstructor->GetNY();
  fNz=fConstructor->GetNZ();
  fOuterRadius_pmt=fConstructor->GetPMTRadius();
  fSphereOn=fConstructor->GetSphereOn();
  fRefl=fConstructor->GetHousingReflectivity();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void NaIQFSetup::PlacePMTs(G4LogicalVolume* pmt_log,
                              G4RotationMatrix *rot,
                              G4double &a, G4double &b, G4double da,
                              G4double db, G4double amin,
                              G4double bmin, G4int na, G4int nb,
                              G4double &x, G4double &y, G4double &z,
                              G4int &k){
/*PlacePMTs : a different way to parameterize placement that does not depend on
  calculating the position from the copy number

  pmt_log = logical volume for pmts to be placed
  rot = rotation matrix to apply
  a,b = coordinates to vary(ie. if varying in the xy plane then pass x,y)
  da,db = value to increment a,b by
  amin,bmin = start values for a,b
  na,nb = number of repitions in a and b
  x,y,z = just pass x,y, and z by reference (the same ones passed for a,b)
  k = copy number to start with
  sd = sensitive detector for pmts
*/
  a=amin;
  for(G4int j=1;j<=na;j++){
    a+=da;
    b=bmin;
    for(G4int i=1;i<=nb;i++){
      b+=db;
      new G4PVPlacement(rot,G4ThreeVector(x,y,z),pmt_log,"pmt",
                        fHouse_log,false,k);
      fPmtPositions.push_back(G4ThreeVector(x,y,z));
      k++;
    }
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void NaIQFSetup::VisAttributes(){

  //PMTs
  G4VisAttributes* pmt_va = new G4VisAttributes(G4Colour(0.0, 1.0, 0.0)); //make PMTs green
  pmt_va -> SetVisibility(true);
  pmt_va->SetForceSolid(true);
  fPhotocath_log -> SetVisAttributes(pmt_va);

  // // make the NaI show up
  // G4VisAttributes* scint_va = new G4VisAttributes(G4Colour(1.0, 1.0, 1.0)); //scint visual att
  // scint_va -> SetVisibility(true);
  // scint_va -> SetForceSolid(true);
  // fScint_log -> SetVisAttributes(scint_va);

  // G4VisAttributes* wrap_va = new G4VisAttributes(G4Colour(0.8,0.8,0.8));
  // ///G4VisAttributes* wrap_va = new G4VisAttributes(G4Colour(0.0, 1.0, 0.0));
  // wrap_va -> SetVisibility(true);
  // wrap_va -> SetForceSolid(true);
  // fWrap_log->SetVisAttributes(wrap_va);

  // G4VisAttributes* shell_va = new G4VisAttributes(G4Colour(0.0, 0.0, 1.0));
  // shell_va -> SetVisibility(true);
  // shell_va -> SetForceSolid(true);
  // fShell_log->SetVisAttributes(shell_va);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void NaIQFSetup::SurfaceProperties(){
  G4double ephotons[] = {1.0*eV, 7.0*eV};
  const G4int num = sizeof(ephotons)/sizeof(G4double);

  //**Scintillator housing properties
  // assume teflon has a consistent reflectivity of 96%
  G4double reflectivity[] = {0.96, 0.96};
  assert(sizeof(reflectivity) == sizeof(ephotons));
  G4double efficiency[] = {0.0, 0.0};
  assert(sizeof(efficiency) == sizeof(ephotons));
  G4MaterialPropertiesTable* scintHsngPT = new G4MaterialPropertiesTable();
  scintHsngPT->AddProperty("REFLECTIVITY", ephotons, reflectivity, num);
  scintHsngPT->AddProperty("EFFICIENCY", ephotons, efficiency, num);
  G4OpticalSurface* OpScintHousingSurface =
    new G4OpticalSurface("HousingSurface",unified,ground,dielectric_metal);
  OpScintHousingSurface->SetMaterialPropertiesTable(scintHsngPT);

  //**Photocathode surface properties
  G4double ephoton[] = {7.0*eV, 7.14*eV};
  G4double photocath_EFF[]={1.,1.}; //Enables 'detection' of photons
  assert(sizeof(photocath_EFF) == sizeof(ephoton));
  G4double photocath_ReR[]={1.92,1.92};
  assert(sizeof(photocath_ReR) == sizeof(ephoton));
  G4double photocath_ImR[]={1.69,1.69};
  assert(sizeof(photocath_ImR) == sizeof(ephoton));
  G4MaterialPropertiesTable* photocath_mt = new G4MaterialPropertiesTable();
  photocath_mt->AddProperty("EFFICIENCY",ephoton,photocath_EFF,num);
  photocath_mt->AddProperty("REALRINDEX",ephoton,photocath_ReR,num);
  photocath_mt->AddProperty("IMAGINARYRINDEX",ephoton,photocath_ImR,num);
  G4OpticalSurface* photocath_opsurf=
    new G4OpticalSurface("photocath_opsurf",glisur,polished,
                         dielectric_metal);
  photocath_opsurf->SetMaterialPropertiesTable(photocath_mt);

  //**Create logical skin surfaces
  new G4LogicalSkinSurface("photocath_surf",fWrap_log,
                           OpScintHousingSurface);
  new G4LogicalSkinSurface("photocath_surf",fPhotocath_log,photocath_opsurf);
}
